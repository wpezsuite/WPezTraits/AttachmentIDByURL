## WPezTraits: Attachment ID By URL

__URL in, WordPress attachment ID - if it exists - returned__ 

As inspired by: https://pippinsplugins.com/retrieve-attachment-id-from-image-url/


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --