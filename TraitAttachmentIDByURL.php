<?php

namespace WPez\WPezTraits\AttachmentIDByURL;

trait TraitAttachmentIDByURL {


    public function attachmentIDByURL( $str_url = false, $mix_return = false ) {

        if ( filter_var( $str_url, FILTER_VALIDATE_URL, FILTER_FLAG_PATH_REQUIRED ) ) {

            // https://pippinsplugins.com/retrieve-attachment-id-from-image-url/
            global $wpdb;
            $arr_attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $str_url ) );

            if ( isset( $arr_attachment[0] ) && is_string( $arr_attachment[0] ) ) {

                $int_attach_id = (integer)$arr_attachment[0];

                return $int_attach_id;
            }
        }

        // e.g., If the look up fails you can pass back a custom value (ID?)
        return $mix_return;
    }
}